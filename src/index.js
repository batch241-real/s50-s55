import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// const name = "Annejanette Real"
// const element = <h1>Hello, {name}</h1>

// const user = {
//   firstName: 'Annejanette',
//   lastName: 'Real'
// };

// function formatName(user){
//   return user.firstName + ' ' + user.lastName;
// };

// const element = <h1>Hello, {formatName(user)}</h1>

// root.render(element);