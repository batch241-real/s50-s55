// import { Link } from 'react-router-dom';
import Banner from '../components/Banner'
// import { Button, Row, Col } from 'react-bootstrap';

export default function Error() {

    const data = {
        title: "Error 404 - Page not found",
        content: "The page you're trying to access cannot be found.",
        destination: '/',
        label: "Back to Homepage"
    }
    return (
    // <Row>
    // 	<Col className="p-5">
    //         <h1>Error 404 - Page not found</h1>
    //         <p>The page you're trying to access cannot be found.</p>
    //         <Link as="Link" to="/">
    //         <Button variant="primary">Back to Homepage</Button>
    //         </Link>
    //     </Col>
    // </Row>
    <>
        <Banner data={data}/>
    </>
	)
}