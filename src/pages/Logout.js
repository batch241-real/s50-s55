import { useContext, useEffect } from 'react';

import { Navigate } from 'react-router-dom';

import UserContext from '../UserContext';

export default function Logout() {

	const { unsetUser, setUser } = useContext(UserContext);
	// localStorage.clear();
	
	// clears the localStorage
	unsetUser();

	useEffect(() => {
		// sets the user state bac to its original value
		setUser({id: null});
	})

	return(
		<Navigate to='/login' />
	)
}